from django.contrib import admin

# Register your models here.
from .models import RequestedService


class RequestedServiceAdmin(admin.ModelAdmin):
    list_display = ('user', 'status', 'location', 'service_type')


admin.site.register(RequestedService, RequestedServiceAdmin)
