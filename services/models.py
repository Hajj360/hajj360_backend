from django.contrib.auth import get_user_model
from django.db import models
from location_field.models.plain import PlainLocationField


# Create your models here.

class ServiceType(object):
    MEDIC = 'MEDIC'
    LOST = 'LOST'
    DEATH = 'DEATH'
    CHOICES = (
        (MEDIC, MEDIC),
        (LOST, LOST),
        (DEATH, DEATH),
    )


class RequestServiceStatus(object):
    SENT = 'SENT'
    IN_PROGRESS = 'IN_PROGRESS'
    COMPLETED = 'COMPLETED'

    CHOICES = (
        (SENT, SENT),
        (COMPLETED, COMPLETED),
        (IN_PROGRESS, IN_PROGRESS),
    )


class RequestedService(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    service_type = models.CharField(choices=ServiceType.CHOICES, max_length=20)
    status = models.CharField(choices=RequestServiceStatus.CHOICES, max_length=20)
    city = models.CharField(max_length=15, blank=True, null=True)
    location = PlainLocationField(based_fields=['city'], zoom=7)
