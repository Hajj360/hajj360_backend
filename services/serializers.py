from django.contrib.auth import get_user_model
from rest_framework import serializers

from services.models import ServiceType


class ServiceSerializer(serializers.Serializer):
    service_type = serializers.ChoiceField(choices=ServiceType.CHOICES)
    user_id = serializers.PrimaryKeyRelatedField(queryset=get_user_model().objects.all())
    location = serializers.CharField(max_length=200)

    def create(self, validated_data):
        """
        """
        pass

    def update(self, instance, validated_data):
        pass
