# Generated by Django 2.0.7 on 2018-08-02 01:53

from django.db import migrations, models
import location_field.models.plain


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0002_requestedservice_location'),
    ]

    operations = [
        migrations.AddField(
            model_name='requestedservice',
            name='city',
            field=models.CharField(default='', max_length=15),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='requestedservice',
            name='location',
            field=location_field.models.plain.PlainLocationField(max_length=63),
        ),
    ]
