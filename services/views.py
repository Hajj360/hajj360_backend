from django.contrib.auth import get_user_model
from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView
from rest_framework import generics
from rest_framework.response import Response

from services.models import RequestedService, RequestServiceStatus
from services.serializers import ServiceSerializer, ServiceType
from users.models import HajjGroup
from users.serilaizers import GroupSerializer


class RequestServiceView(generics.CreateAPIView):
    serializer_class = ServiceSerializer

    def create_request_service(self, user_id, service_type, location):
        requested_service, created = RequestedService.objects.get_or_create(user_id=user_id,
                                                                            service_type=service_type,
                                                                            status=RequestServiceStatus.SENT)
        if location:
            requested_service.location = location
            requested_service.save()
        return requested_service

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer_class()(data=request.data)
        if serializer.is_valid():
            service_type = serializer.data.get('service_type')
            user_id = serializer.data.get('user_id')
            location = serializer.data.get('location')
            if service_type == ServiceType.MEDIC:
                requested_service = self.create_request_service(user_id, service_type, location)
            elif service_type == ServiceType.DEATH:
                requested_service = self.create_request_service(user_id, service_type, location)

            return Response({'status': requested_service.status,
                             'message': 'Your request for help has been sent with your current location.'})
        else:
            return Response(serializer.errors)


class GetGroupLocation(generics.CreateAPIView):
    serializer_class = ServiceSerializer

    def post(self, request, *args, **kwargs):
        user_id = request.data.get('user_id', None)
        try:
            user = get_user_model().objects.prefetch_related('hajj_groups').get(pk=user_id)
            requested_service, created = RequestedService.objects.get_or_create(user_id=user_id,
                                                                                service_type=ServiceType.LOST,
                                                                                status=RequestServiceStatus.SENT)
            serializer = GroupSerializer(instance=user.hajj_groups.all(), many=True)
            response = serializer.data
            status = 200
        except Exception as e:
            status = 404
            response = {'message': "Not Found"}
        return Response(response, status=status)


class ListRequestedService(ListView):
    template_name = 'requested_service_list.html'
    model = RequestedService
    paginate_by = 100  # if pagination is desired

    def get_queryset(self):
        qs = super(ListRequestedService, self).get_queryset()
        if self.request.GET.get('service_type'):
            qs = qs.filter(service_type=self.request.GET.get('service_type'))
        return qs
