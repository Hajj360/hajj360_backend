from django.urls import path

from . import views

urlpatterns = [
    path('request', views.RequestServiceView.as_view()),
    path('find_group', views.GetGroupLocation.as_view()),
    path('list', views.ListRequestedService.as_view()),
]
