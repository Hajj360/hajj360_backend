import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models

from location_field.models.plain import PlainLocationField


# Create your models here.
def unique_location_key():
    return str(uuid.uuid4()).replace('-', '')[:6]


class CustomUser(AbstractUser):
    access_code = models.CharField(max_length=5, unique=True)
    city = models.CharField(max_length=15, blank=True, null=True)
    last_location = PlainLocationField(based_fields=['city'], zoom=7)
    location_key = models.CharField(max_length=6, unique=True, null=True, blank=True, default=unique_location_key)

    def save(self, *args, **kwargs):
        if not self.location_key:
            self.location_key = unique_location_key()
        return super(CustomUser, self).save(*args, **kwargs)


class HajjGroup(models.Model):
    group_name = models.CharField(max_length=6)
    owner = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='owner_groups')
    users = models.ManyToManyField(CustomUser, related_name='hajj_groups')
