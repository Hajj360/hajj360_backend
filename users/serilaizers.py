from rest_framework import serializers

from users.models import CustomUser, HajjGroup


class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=False)
    access_code = serializers.CharField(required=False)
    last_location = serializers.CharField(required=False)

    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'access_code', 'email', 'last_location', 'location_key')


class GroupSerializer(serializers.ModelSerializer):
    owner = UserSerializer()

    class Meta:
        model = HajjGroup
        fields = ('id', 'group_name', 'owner', 'users')
