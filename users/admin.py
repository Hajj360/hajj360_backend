from django.contrib import admin

# Register your models here.
from users.models import CustomUser, HajjGroup


class UserAdmin(admin.ModelAdmin):
    pass


admin.site.register(CustomUser, UserAdmin)


class HajjGroupAdmin(admin.ModelAdmin):
    pass


admin.site.register(HajjGroup, HajjGroupAdmin)
