# Generated by Django 2.0.7 on 2018-08-01 20:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_hajjgroup'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='last_location',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='customuser',
            name='access_code',
            field=models.CharField(max_length=5, unique=True),
        ),
    ]
