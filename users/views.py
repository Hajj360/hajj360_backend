from django.contrib.auth import get_user_model

# Create your views here.
from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from users.serilaizers import UserSerializer


@api_view(['POST'])
def get_user_by_access_code(request):
    data = request.data
    access_code = data.get('code')
    try:
        user = get_user_model().objects.get(access_code=access_code)
        serializer = UserSerializer(instance=user)
        return Response(serializer.data)
    except Exception as e:
        return Response({"Not found"}, status=404)


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('location_key',)
