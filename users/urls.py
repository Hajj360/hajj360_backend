from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views
router = DefaultRouter()
router.register(r'', views.UserViewSet)
urlpatterns = [
    path('users/get_info', views.get_user_by_access_code),
    # path('', views.UserViewSet),
]
